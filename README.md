# Sentiment Analysis
This project deals with a sentiment analysis of customer reviews.
We used Tensorflow to build our Deep Learning model and Tensorflow Hub to implement a word embedding.
Our trained model has a F1-Score of 80,43%.

## Jupyter Notebook
We recommend to install [Anaconda](https://www.anaconda.com/products/individual) and to use Jupyter Notebook 

1. Install Anaconda
2. Create a virtual envirement with Anaconda
3. Install the Jupyter Notebook, Tensorflow, Tensorflow Hub and Pandas in your virtual envirement


- pip install jupyter notebook
- pip install tensorflow 
- pip install tensorflow-hub
- pip install pandas

4. Launch Jupyter Notebook in your virtual envirement and open the Sentiment Analysis Notebook
5. Run the code in the notebook

How to use Tensorboard:
1. Start a terminal in your virtual envirement
2. Navigate to the project folder
3. Enter the command tensorboard --logdir=logs/history
4. Open your favorite browser and enter localhost:6006



## Google Colab
1. Open [Colab](https://colab.research.google.com)
2. Sign in with your Google Account
3. Upload the Sentiment_Analysis_Google_Collab Notebook
4. Upload the files from the resources directory to your Google Drive
5. Change the paths to the right files in cell 3 and 4 in the notebook
6. Run the code in the notebook
